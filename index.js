const mysql = require('mysql');
const MySQLEvents = require('@rodrigogs/mysql-events');

const dialogflow = require('dialogflow');
const uuid = require('uuid');

const projectId = 'bpolicial';
const sessionId = uuid.v4();
const languageCode = 'es';
const sessionClient = new dialogflow.SessionsClient();

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'moodledude',
    password: 'm00dl3Dude#',
    database: 'moodle'
});

const instance = new MySQLEvents(connection, {
    startAtEnd: true,
    excludedSchemas: {
        mysql: true,
    },
});

const adaBot = 4;
var affectedRow = '';

async function detectIntent(
    projectId,
    sessionId,
    query,
    contexts,
    languageCode
) {
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: query,
                languageCode: languageCode,
            },
        },
    };

    if (contexts && contexts.length > 0) {
        request.queryParams = {
            contexts: contexts,
        };
    }
    const responses = await sessionClient.detectIntent(request);
    return responses[0];
}

async function executeQueries(projectId, sessionId, queryText, languageCode) {
    let context;
    let intentResponse;
    try {
        console.log(`Sending Query: ${queryText}`);
        intentResponse = await detectIntent(
            projectId,
            sessionId,
            queryText,
            context,
            languageCode
        );
        context = intentResponse.queryResult.outputContexts;
        var intentText = intentResponse.queryResult.fulfillmentText;
        console.log(affectedRow.conversationid + ', to ' + affectedRow.useridfrom + ' from ' + adaBot);
        var dateNow = Date.now();
        connection.query('INSERT INTO mdl_messages (id, useridfrom, conversationid, subject, fullmessage, fullmessageformat, fullmessagehtml, smallmessage, timecreated) VALUES (NULL,' + adaBot + ', ' + affectedRow.conversationid + ', null, \'' + intentText + '\', 0, NULL, \'' + intentText + '\', ' + parseInt(dateNow / 1000) + ')', function (error, result, fields) {
            if (error) throw error;
        });

    } catch (error) {
        console.log(error);
    }
}

const program = async () => {
    await instance.start();

    instance.addTrigger({
        name: 'TEST',
        expression: 'moodle.mdl_messages',
        statement: MySQLEvents.STATEMENTS.ALL,
        onEvent: (event) => {
            affectedRow = event.affectedRows[0].after;
            var botQuery = 'SELECT * FROM mdl_message_conversation_members WHERE userid=' + adaBot + ' AND conversationid=' + affectedRow.conversationid;
            connection.query(botQuery, function (error, result, fields) {
                if (error) throw error;
                if (result.length > 0 && affectedRow.useridfrom != adaBot) {
                    executeQueries(projectId, sessionId, affectedRow.fullmessage, languageCode);
                }
            });
        },
    });

    instance.on(MySQLEvents.EVENTS.CONNECTION_ERROR, console.error);
    instance.on(MySQLEvents.EVENTS.ZONGJI_ERROR, console.error);
};

program()
    .then(() => console.log('Waiting for database events...'))
    .catch(console.error);