# ![](./pix/icon.png "ADABot Socket") ADABot Socket

Socket de interacción entre MySQL y DialogFlow para la operación de ADABot.

## Configuración

### MySQL

Habilitar MySQL binlog en `my.cnf` y reiniciar el servicio.

    # Must be unique integer from 1-2^32
    server-id        = 1
    # Row format required for ZongJi
    binlog_format    = row
    # Directory must exist. This path works for Linux. Other OS may require
    #   different path.
    log_bin          = /var/log/mysql/mysql-bin.log

    binlog_do_db     = employees   # Optional, limit which databases to log
    expire_logs_days = 10          # Optional, purge old logs
    max_binlog_size  = 100M        # Optional, limit log size

También se deben conceder los siguientes permisos a un usuario de la base de datos.

    GRANT REPLICATION SLAVE, REPLICATION CLIENT, SELECT ON *.* TO 'moodledude'@'localhost';


### NodeJS

Ejecutar el comando `npm install`.

Parámetros para GCP, projectId en consola.

    const projectId = '';

Parámetros de conexión de la base de datos.

    const connection = mysql.createConnection({
        host: '',
        user: '',
        password: '',
        database: ''
    });

Proporcionar `userid` del usuario destinado a funcionar como __ADABot__ en Moodle.

    const adaBot = 4;

### Google Cloud Platform

Se debe instalar el __google-cloud-sdk__ que se encuentra en la [Google Cloud SDK documentation](https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-275.0.0-linux-x86_64.tar.gz) y después exportar las credenciales.

    export GOOGLE_APPLICATION_CREDENTIALS="[PATH]"